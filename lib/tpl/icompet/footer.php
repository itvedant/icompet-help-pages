
<!--footer-->
<div class="footer">
    <div class="row-fluid">
            <div class="span4 text-center">
                <div class="span12">
                    <p class="fcall">call us on <a href="tel:9967708111">91-9967708111</a></p>
                </div>
                <div class="row-fluid">
                    <div class="span6">
                        <p class="ftitle"><u>QUICKLINKS</u></p>
                        <ul class="flink" type="none">
                            <a href="#"><li>about us</li></a>
                            <a href="http://icompet.com/blog/"><li>our blog</li></a>
                            <a href="#"><li>contest</li></a>
                            <a href="#"><li>gallery</li></a>
                        </ul>
                    </div>
                    <div class="span6">
                        <p class="ftitle"><u>SUPPORT</u></p>
                        <ul class="flink" type="none">
                            <a href="#"><li>FAQ</li></a>
                            <a href="#"><li>help</li></a>
                            <a href="#"><li>terms and condition</li></a>
                            <a href="#"><li>privacy policy</li></a>
                            <a href="#"><li>contact us</li></a>
                        </ul>
                    </div>
                </div>
            </div>
			<div class="span4 text-center">
                    <div>
                        <img src="/img/flogo.png"/>
                    </div>
                    <div>
                        <p class="font follow">FOLLOW US ON</p>
                    </div>
                    <div class="foot-social-grp">
                        <a href="https://www.facebook.com/icompet" target="_blank" class="fb-footer"></a>
                        <a href="https://twitter.com/icompet" target="_blank" class="tw-footer"></a>
                        <a href="https://plus.google.com/102044210813844742480/about" target="_blank" class="gmail-footer"></a>
                        <a href="https://www.youtube.com/channel/UCyYvv7JVM1frBe_iHddCOvg" target="_blank" class="yt-footer"></a>
						<div class="clear"></div>
                    </div>
			</div>
            <div class="span4">
        </div>


    <div class="row-fluid">
        <div class="span12"><p class="cright tac">Copyright &#169; <?php echo date('Y');?> icompet.com. </p></div>
    </div>
</div>