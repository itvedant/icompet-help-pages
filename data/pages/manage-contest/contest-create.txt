====== Creating a contest ======
  - Once you are logged in go to the administrator's dashboard
  - Click on the create a contest button
  - Follow the wizard, fill in basic contest details first
  - Add Prizes, Judges, Sponsors for the contest
  - Publish the contest freely or choosing any of the payment options available

{{ :manage-contest:screen_shot_2014-10-09_at_8.07.06_pm.png?direct&500 |}}