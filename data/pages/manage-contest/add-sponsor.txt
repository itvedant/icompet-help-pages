===== Add Sponsor =====
Adding a sponsor to your organization adds the gratitude and credits if your contest is organized in collaboration with other members.A sponsor may be a person or a Company whom you are collaborating to organize a contest.

===== To add a new sponsor =====
  - Fill in the sponsorer's name
  - Optionally upload the display picture of the sponsor's organization or the person's pic by clicking on the image beside.
  - Optionally add in some extra description about the sponsorer so that your audience can know the information about them.
  - Click **Add Sponsor** to add the sponsor to the current contest

{{:manage-contest:screen_shot_2014-10-09_at_10.05.04_pm.png?direct&300|Adding a Sponsor}}