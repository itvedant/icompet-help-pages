====== Embeddding contest on your website ======
Your contest can be embedded onto any website using the embed code.
  - Open the contest which you want to embed into other website
  - From the list of menu navigation tabs choose **embed**
  - You have  3 options
    - Object Embed Code
    - IFrame Embed Code
    - Responsive website Embed Code
  - click copy to copy any of the code
  - paste the copied code to your website's source file
