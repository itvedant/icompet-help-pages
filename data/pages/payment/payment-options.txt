===== Payment Options ===== 
Payment to icompet can be done only through credit as well as debit or paypal.
The following cards are accepted:

{{http://www.2checkout.com/upload/images/paymentlogoshorizontal.png|icompet payment options}}

We accept the following payments
  - Visa
  - Master card
  - American express
  - Paypal
  - Diners
  - JCB
  - PIN debit cards with the Visa or MasterCard logo
  - Debit cards with the Visa or MasterCard logo

