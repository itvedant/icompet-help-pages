===== Add Judges =====
Judges are very important aspect of your contest. A healthy competition is motivated amongst its participants with impartial judges.You can specify designation like CEO of XYZ Organization, Principal at Xaviers etc.Prominent judges are one's with good positions.

===== To add a new judge =====
  - Fill in the full name of the judge
  - Optionally add the designation / position of the judge
  - Optionally upload the display picture of the judge by clicking on the image beside.
  - Optionally add in some extra description about the judge so that your audience can know the information about his background or achievements.
  - Click **Add Judge** to add the judge to the current contest

{{:manage-contest:screen_shot_2014-10-09_at_9.36.55_pm.png?direct&300|Add a Judge to Contest }}

{{:manage-contest:screen_shot_2014-10-09_at_9.39.39_pm.png?direct&300|View after judge is added to the contest}}