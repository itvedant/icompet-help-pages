====== Selection of Winners ======
Judges as well as administrators have the permission to select the winner from the winner selection section of your contest.
To select a winner you must have first added appropriate prizes.Winners are allocated to prizes. 
See the demo sreen where only one prize was allocated hence only one option is availaible.

{{:judging:invitation.png?300|Winner Selection}}